import numpy as np
import svgelements
from svgelements import SVG
import os
import csv
import path as p
import matplotlib.pyplot as plt
import re

dirname = os.path.dirname(__file__)
all_commands = ['m', 'M', 'l', 'L', 'c', 'C', 'q', 'Q', 'z', 'Z']

"""
SVG Parser
@author Diego Miranda
"""


def coord_str_to_float_arr(coord_str):
    """
    Converts a String containing comma-separated coordinates into a list of
    floating-point coordinates.

    :param coord_str: A string containing comma-separated coordinates.
    :rtype: A list containing floating-point coordinates.
    """
    coord_str = coord_str.split(",")
    coords = list()
    for c in coord_str:
        coords.append(float(c))  # make a list of floats

    return coords


def parse_close(coord_args):
    """
    Parses a Close command and identifies its endpoint.

    :param coord_args: An list of a single comma-separated String, e.g. ["0,0"]
    :rtype: Close
    """
    if len(coord_args) < 1:
        return p.Close()

    coords = coord_str_to_float_arr(coord_args[0])  # returns [x, y]
    # print("Line added: Coords: {}".format(coords))
    return p.Close(coord=(coords[0], coords[1]))


def parse_line(coord_args):
    """
    Parses a Move command and identifies its endpoint.

    :param coord_args: An list of a single comma-separated String, e.g. ["0,0"]
    :rtype: Line
    """
    coords = coord_str_to_float_arr(coord_args[0])  # returns [x, y]

    # print("Line added: Coords: {}".format(coords))
    return p.Line(coord=(coords[0], coords[1]))


def parse_move(coord_args):
    """
    Parses a Move command and identifies its endpoint.

    :param coord_args: An list of a single comma-separated String, e.g. ["0,0"]
    :rtype: Move
    """
    coords = coord_str_to_float_arr(coord_args[0])  # returns [x, y]

    # print("Move added: Coords: {}".format(coords))
    return p.Move(coord=(coords[0], coords[1]))


def parse_cubic_bezier(cubic_args):
    """
    Parses a cubic Bezier and identifies its endpoint and anchors.

    :param cubic_args: A list of 3 comma-seperated coordinate Strings, e.g. ["0,0", "1,1", "2,2"]
    :rtype: CubicBezier
    """
    assert (cubic_args is not None)
    anchor1 = coord_str_to_float_arr(cubic_args[0])  # returns [x, y]
    anchor2 = coord_str_to_float_arr(cubic_args[1])  # returns [x, y]
    coords = coord_str_to_float_arr(cubic_args[2])  # returns [x, y]

    # print("Cubic Bezier added: Anchor 1: {}\tAnchor2: {}\t Coords: {}".format(anchor1, anchor2, coords))
    return p.CubicBezier((coords[0], coords[1]),
                         (anchor1[0], anchor1[1]),
                         (anchor2[0], anchor2[1]))


def parse_quadratic_bezier(quad_args):
    """
    Parses a quadratic Bezier and identifies its endpoint and anchor.

    :param quad_args: A list of 2 comma-seperated coordinate Strings, e.g. ["0,0", "1,1"]
    :rtype: QuadraticBezier
    """
    assert (quad_args is not None)
    anchor = coord_str_to_float_arr(quad_args[0])  # returns [x, y]
    coords = coord_str_to_float_arr(quad_args[1])  # returns [x, y]

    # print("Quadratic Bezier added: Anchor: {}\t Coords: {}".format(anchor, coords))
    return p.QuadraticBezier((coords[0], coords[1]),
                             (anchor[0], anchor[1]))


def parse_subpath_segment(segment) -> p.Segment:
    """
    Takes in a SEGMENT of a PATH object and returns a custom SEGMENT object

    :arg: segment: A String containing a command and its arguments, .e.g. "m 0,0"
    :rtype: Segment object
    """
    # print("DEBUG {}\n".format(segment))
    cmd_type = segment[0]  # first element is the command type
    cmd_args = segment[1:]  # the rest are its arguments
    # print("command type: {} args: {}".format(cmd_type, segment))

    cmd_args = cmd_args.split(" ")  # split String of command arguments into list of Strings
    while "" in cmd_args:
        cmd_args.remove("")  # remove blanks...

    # print("Segment argument array: {}".format(segment))

    segment = None
    # Parse commands according to type.
    if cmd_type == "m":
        segment = parse_move(cmd_args)
    elif cmd_type == "l":
        segment = parse_line(cmd_args)
    elif cmd_type == "c":
        segment = parse_cubic_bezier(cmd_args)
    elif cmd_type == "z":
        segment = parse_close(cmd_args)
    elif cmd_type == "q":
        segment = parse_quadratic_bezier(cmd_args)
    else:
        raise LookupError("Could not parse subpath segment of type {}".format(cmd_type))

    return segment


def split_subpath(subpath):
    """
    Splits a given subpath into an array of path commands
    :arg: subpath: a Subpath string or object

    :return: an array of subpaths
    """
    if not isinstance(subpath, str):
        subpath = str(subpath)

    subpath_segments = list()
    prev = 0
    curr = 0
    for chars in list(subpath):
        if chars in all_commands:
            if chars == "z":
                subpath_segments.append(subpath[curr:])
            else:
                subpath_segments.append(subpath[prev:curr])
            prev = curr
        curr += 1

    subpath_segments.remove("")
    return subpath_segments


def parse_elements(svg) -> list:
    elements = list(svg.elements())

    parsed_subpaths = list()
    for element in elements:
        # print(element)
        if isinstance(element, svgelements.Path):
            # print("Path element: {}".format(element))

            subpaths = list(element.as_subpaths())
            subpath_index = 0
            for subpath in subpaths:
                print("SVG Subpath found: {}".format(subpath))
                parsed_subpaths.append(list())
                subpath_segments = split_subpath(subpath)

                for segment in subpath_segments:
                    parsed_subpaths[subpath_index].append(parse_subpath_segment(segment))

                subpath_index += 1

    return parsed_subpaths


def generate_subpath(segments_list) -> p.Subpath:
    """
    Generates a Subpath object given a list of SVG-formatted Segment objects

    :param segments_list: a list containing Segment objects
    :rtype: Subpath
    """
    subpath = p.Subpath()

    for segment in segments_list:
        subpath.add_segment(segment)

    return subpath.calculate_subpath()


def parse_image(file):
    """
    Parses a given SVG file and returns a tuple of x and y lists, each of which
    are a list of lists. Each sub-list represents a subpath.
    :param file: filepath to an SVG file
    :return: ([x[]], [y[]])
    """

    svg = SVG.parse(file)   # open SVG file
    parsed_subpaths_list = parse_elements(svg)  # obtain list of subpaths in SVG format

    # x y points buffers
    x_points = list()
    y_points = list()

    # parse all SVG-formatted subpaths and their segments
    for segments_list in parsed_subpaths_list:
        subpath_x, subpath_y = generate_subpath(segments_list)  # obtain discrete points of a subpath

        # convert float array into int array (faster processing)
        # subpath_x = list(map(int, subpath_x))
        # subpath_y = list(map(int, subpath_y))

        # add points to buffers
        x_points.append(subpath_x)
        y_points.append(subpath_y)

    # flattens list of lists into one list...
    x_points = np.concatenate(x_points)
    y_points = np.concatenate(y_points)
    x = np.array(x_points).flatten()
    y = np.array(y_points).flatten()

    # get name of the file being parsed
    match = re.search(r"/.+?(?=\.svg)", file.__str__())
    if match is None:
        raise ValueError("Could not parse name of file...")

    csv_filename = match.group(0) + ".csv"
    # Open a CSV file and write the x,y data points
    with open(csv_filename, 'w', newline='') as csvfile:
        # Create a writer object
        writer = csv.writer(csvfile)

        # Write the data to the file
        for xi, yi in zip(x, y):
            writer.writerow([xi, yi])
        print("Successfully saved x and y data points to {}\n".format(csv_filename))

    # ultimately return the flattened subpath data points...
    return x, y


def run_parser(optional_files=None, display_plot=True):
    """
    Runs the SVG parser and outputs CSV files for a set of default SVG files, specifically
    a square, a curve, and the Duke "D".
    :param display_plot: matplotlib graph display on or off
    :param optional_files: A list of strings with the relative filepath of any optional data files.
    :return: None. all data is stored in a CSV file.
    """
    # defines default files, with option to add more from function arguments.
    files = ["data/square.svg", "data/curve.svg", "data/duke.svg"]
    if optional_files:
        files = files + optional_files

    # Create a figure with three subplots
    fig, axs = plt.subplots(nrows=1, ncols=3, figsize=(10, 5))
    # Loop over each file and plot the corresponding data
    for i, f in enumerate(files):

        # check if data file format is valid
        if f.rsplit(".", 1)[-1] != "svg":
            raise TypeError("Expected SVG file. Instead got {}".format(f))

        # now try to parse the file
        try:
            filepath = os.path.join(dirname, f)
            print("Beginning SVG parsing operation on {}".format(f))
            x, y = parse_image(filepath)
            axs[i].set_aspect("equal")
            axs[i].plot(x, y)

        except FileNotFoundError as err:
            print(err)

    # Display the plots
    if display_plot:
        plt.show()


run_parser()

# Notes:
# SVG Path standard https://www.w3.org/TR/svg-paths/
# Quadratic Bezier https://towardsdatascience.com/bézier-curve-bfffdadea212
