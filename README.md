# SVG Parser
### Diego Miranda


## Purpose

This parser class was built for my _ECE 383: Introduction to Robotics Course_
final project. For our final project, we were tasked to create out own
project that utilized a Roomba and its sensors to solve a task. The task we 
chose to solve was to have it draw out an image using motion control and waypoints.
Thus, our solution involved parsing an SVG image to generate a list of points
for the Roomba to take in and follow.

## Usage

A simple `import parser` statement will execute the main `run_parser()` method
within `parser.py`. This will generate points for three files: a square, a curve,
and the Duke 'D'. If more files are desired to be parsed, `run_parser()` can be ran again with 
a list of filepaths to the SVGs in the arguments.


## Requirements

- `svgelements`