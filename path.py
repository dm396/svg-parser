from abc import ABC, abstractmethod
import numpy as np
import matplotlib.pyplot as plt


class Point:
    point = ()

    def __init__(self, x, y):
        self.point = (x, y)

    def get_point(self):
        return self.point


class Segment:

    @abstractmethod
    def get_discrete_points(self, start, ppm):
        print("discrete points")

    @abstractmethod
    def segment_type(self) -> str:
        return "segment_type"


class Line(Segment):
    coord = (0, 0)

    def __init__(self, coord):
        assert (coord is not None)
        self.coord = coord

    def get_discrete_points(self, start=None, ppm=1):
        """
        Outputs the next point for the Roomba to go to. Since it is a line, it will not return an
        array of points, as that would be redundant for its control.
        :param start: The absolute starting position of the line.
        :param ppm: points per meters
        :rtype: float list
        """
        assert (start is not None)
        points_list = list()
        endpoint = ((start[0] + self.coord[0]) * ppm, (start[1] + self.coord[1]) * ppm)
        points_list.append(endpoint)
        # print("Line calculated points: {}".format(points_list))
        return points_list

    def __str__(self):
        return "Line: p {}".format(self.coord)

    def segment_type(self) -> str:
        return "Line"


class Move(Segment):
    coord = (0, 0)

    def __init__(self, coord):
        assert (coord is not None)
        self.coord = coord

    def get_discrete_points(self, start=None, ppm=1):
        """
        Outputs the next point for the Roomba to go to. Since it is a line, it will not return an
        array of points, as that would be redundant for its control.
        :param start:
        :param ppm: points per meters
        :rtype: float tuple
        """
        if start is None:  #
            return self.coord
        else:
            return start + self.coord

    def __str__(self):
        return "Move: p {}".format(self.coord)

    def segment_type(self) -> str:
        return "Move"


class QuadraticBezier(Segment):
    """
    Represents a Quadratic Bézier curve, which is defined by two anchor points and an endpoint.
    The start point is based off the segment appended before the curve.
    """
    coord = (0, 0)
    anchor = (0, 0)

    def __init__(self, coord, anchor):
        self.coord = coord
        self.anchor = anchor

    def get_discrete_points(self, start, ppm=1):
        assert (start is not None)
        # points = list()

        # calculate the absolute position of these points...
        end = (start[0] + self.coord[0], start[1] + self.coord[1])
        a = (start[0] + self.anchor[0], start[1] + self.anchor[1])

        # determine total number of samples for x and y points...
        args_list = list(start) + list(end) + list(a)
        samples = int(np.max(args_list) - np.min(args_list))

        # Create a linspace for the t variable
        t = np.linspace(0, 1, samples)

        # Calculate the x and y components of the Bézier curve
        # using the quadratic equation:
        # x0(1-t)^2 + b*2t(1-t) + x1*t^2
        x = start[0] * np.power(1 - t, 2) + \
            a[0] * 2 * t * (1 - t) + \
            end[0] * np.power(t, 2)

        # y0(1-t)^2 + b*2t(1-t) + y1*t^2
        y = start[1] * np.power(1 - t, 2) + \
            a[1] * 2 * t * (1 - t) + \
            end[1] * np.power(t, 2)

        # Create a list of (x, y) points along the Bézier curve
        points = list(zip(x, y))

        # Append the end point to the list of points
        points.append(end)

        # Return the list of points
        return points


class CubicBezier(Segment):
    """
    Represents a Cubic Bézier curve, which is defined by two anchor points and an endpoint.
    The start point is based off the segment appended before the curve.
    """
    coord = (0, 0)
    anchor1 = (0, 0)
    anchor2 = (0, 0)

    def __init__(self, coord, anchor1, anchor2):
        self.coord = coord
        self.anchor1 = anchor1
        self.anchor2 = anchor2

    def get_discrete_points(self, start, ppm=1):
        assert (start is not None)
        # points = list()

        # calculate the absolute position of these points...
        end = (start[0] + self.coord[0], start[1] + self.coord[1])
        a1 = (start[0] + self.anchor1[0], start[1] + self.anchor1[1])
        a2 = (start[0] + self.anchor2[0], start[1] + self.anchor2[1])

        # determine total number of samples for x and y points...
        args_list = list(start) + list(end) + list(a1) + list(a2)
        samples = int(np.max(args_list) - np.min(args_list))

        # create a linspace for the t variable
        t = np.linspace(0, 1, samples)

        # calculate x and y components of Bezier
        # x0(1-t)^3 + a1*3t(1-t)^2 + a2*3t^2(1-t) + x1*t^3
        x = start[0] * np.power(1 - t, 3) + \
            a1[0] * 3 * t * np.power(1 - t, 2) + \
            a2[0] * 3 * np.power(t, 2) * (1 - t) + \
            end[0] * np.power(t, 3)

        # y0(1-t)^3 + a1*3t(1-t)^2 + a2*3t^2(1-t) + y1*t^3
        y = start[1] * np.power(1 - t, 3) + \
            a1[1] * 3 * t * np.power(1 - t, 2) + \
            a2[1] * 3 * np.power(t, 2) * (1 - t) + \
            end[1] * np.power(t, 3)

        # samples))
        points = list(zip(x, y))
        points.append(end)
        return points

    def __str__(self):
        return "Cubic Bezier: p {}\t a1{}\t a2{}".format(self.coord, self.anchor1, self.anchor2)

    def segment_type(self) -> str:
        return "Cubic"


class Close(Segment):
    coord = (0, 0)

    def __init__(self, coord=None):
        if coord is not None:
            self.coord = coord

    def get_discrete_points(self, start=None, ppm=1):
        points_list = list()

        if self.coord == (0, 0):  # return-to point is same as the first "Move" point.
            points_list.append(self.coord)
            return points_list

    def __str__(self):
        return "Close: p {}".format(self.coord)

    def segment_type(self) -> str:
        return "Close"


class Subpath:
    segments = list()
    start_segment = None
    end_segment = None

    def __init__(self):
        """
        Initializes blank list of segments
        """
        self.segments = list()

    def add_segment(self, segment):
        """
        Adds a segment to the subpath's list of segments, and/or defines the starting
        and ending segment information.
        :param segment: a Segment object, such as a Line, Curve, or Move/Close command.
        """
        if segment.__class__ == Move:
            # print("Added start:  \t{}".format(segment))
            self.start_segment = segment
        elif segment.__class__ == Close:
            # print("Added close:  \t{}".format(segment))
            self.end_segment = segment
        else:
            # print("Added segment:\t{}".format(segment))
            self.segments.append(segment)

    def calculate_subpath(self):
        """
        Parses its segments to return a tuple (x, y) of lists which contains discrete path points
        :return: ([x], [y])
        """
        assert (self.segments is not None)
        assert (self.start_segment.__class__ is Move)
        assert (self.end_segment.__class__ is Close)

        subpath_points = list()

        starting_point = self.start_segment.get_discrete_points()  # tuples (x0, y0)
        subpath_points = subpath_points + [starting_point]

        # print("Subpath starting point: {}".format(starting_point))
        previous_end = starting_point
        for s in self.segments:
            segment_points = s.get_discrete_points(start=previous_end)
            # print("more points!! {}".format(segment_points))
            subpath_points = subpath_points + segment_points
            # print("current list of points: {}".format(subpath_points))
            # print("segment points {}".format(segment_points))
            previous_end = segment_points[-1]

        subpath_points = subpath_points + [starting_point]

        xy = list(map(list, zip(*subpath_points)))
        x = xy[0]
        y = xy[1]

        return x, y
